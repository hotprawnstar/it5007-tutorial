


var span = document.getElementById('clock');

function clock() {
  var d = new Date();
  var s = d.getSeconds();
  var m = d.getMinutes();
  var h = d.getHours();
  span.innerHTML = d.toDateString() + " " +
    ("0" + h).substr(-2) + ":" + ("0" + m).substr(-2) + ":" + ("0" + s).substr(-2);
}

setInterval(clock, 1000);







let page = 0; // 0 == homepage 1 == waitlist
let display_table = 0; // 0 = displayed 1 = hidden


document.getElementById("addGuest").hidden = true;
document.getElementById("waitListDisplay").hidden = true;
document.getElementById("waitListDisplayBtn").hidden = true;
const navigate = document.querySelector(".navigateBtn");
const add = document.querySelector(".guestAdd");
const checkbox = document.querySelector(".delete");
const remove = document.querySelector(".removeGuest");
const display = document.querySelector(".waitListDisplayBtn");
display.addEventListener('click', displayTable);
navigate.addEventListener('click', changePage);
// populateTable();

// function populateTable(){
//     document.getElementById("waitListDisplay").innerHTML=
//     "<table id = displayTable>"
//     + "<tr><td>ID</td><td>Guest Name</td><td>Guest Contact Number</td>"
//     + "<td> Created Date </td></tr><table>"
//     ;
// }

function displayTable(){
    if (display_table == 0){
        document.getElementById("waitListDisplay").hidden = true;
        display_table = 1;
    }else
    {
        document.getElementById("waitListDisplay").hidden = false;       
        display_table = 0;
    }

}

function changePage()
{
    if (page == 0){
    document.getElementById("homeMetric").hidden = true;
    page = 1;
    document.getElementById("pagetitle").innerHTML = "<span>Hotel California Reservation Management System - Wait List</span>";
    document.getElementById("addGuest").hidden = false;
    document.getElementById("Navigate").value = "Go to Home Page!";
    document.getElementById("waitListDisplay").hidden = false;
    document.getElementById("waitListDisplayBtn").hidden = false;
    // if(waitList.length > 0){
    //     populateWaitList();
    //     document.getElementById("emptyTable").hidden = true;
    // }
} else{
    document.getElementById("homeMetric").hidden = false;
    document.getElementById("pagetitle").innerHTML = "<span>Hotel California Reservation Management System - Home</span>";
    page = 0;
    document.getElementById("addGuest").hidden = true;
    document.getElementById("Navigate").value = "Go to Wait List!";
    document.getElementById("waitListDisplay").hidden = true;
    document.getElementById("waitListDisplayBtn").hidden = true;


}
}

function removeSelection(){
    if (waitList.length == 0){
        alert("No customer on wait list!");
    }
    else
    {
        
    let check = 0;
    let check_array = [];
    for (let i = 0; i < waitList.length; i++){
        if(document.getElementById(i).checked){
            check_array.push(waitList[i]);
            check+=1;
        }
    }
    if (check == 0){
        alert("Select a customer first!");
    }else
    {
    waitList = waitList.filter(item => !check_array.includes(item));
    populateWaitList();    
    if (waitList.length == 0){
        document.getElementById("emptyTable").hidden = false;
    }
    localStorage.setItem("waitList", JSON.stringify(waitList));
}
    }
}

function populateWaitList(){
    output=""
    for (let i = 0; i < waitList.length; i++){
        output += "<tr><td><input type = checkbox class = delete id = "+i
        +" value = "+i+"></td>";
        output+="<td>"+(i+1)+"</td>";
        output+="<td>"+waitList[i][1]+"</td>";
        output+="<td>"+waitList[i][2]+"</td>";
        output+="<td>"+waitList[i][3]+"</td>";
        output+="</tr>";

    }
    document.getElementById("waitListBody").innerHTML = output;
}

// const guestAdd = document.querySelector(".guestAdd");
//     guestAdd.addEventListener('click', addGuest);

//     function addGuest()
//     {
//         alert("Guest Name:" +  document.querySelector(".guestName").value + '\n' +"Guest Number:" +  document.querySelector(".guestNum").value );

//     }