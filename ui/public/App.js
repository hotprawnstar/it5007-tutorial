class DisplayLabel extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("label", {
      htmlFor: this.props.for
    }, " ", this.props.content);
  }

}

class DisplayCheckBox extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("input", {
      type: "checkbox",
      className: this.props.cbClass,
      id: this.props.cbID,
      value: this.props.cbValue,
      onChange: this.props.cbChange
    });
  }

}

class DisplayTextBox extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("input", {
      type: "text",
      id: this.props.id,
      name: this.props.name,
      className: this.props.tbClass
    });
  }

}

class AddCustomer extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const form = document.forms.addEntry;
    const entry = {
      name: form.guestName.value,
      number: form.guestNum.value,
      date: new Date()
    };
    this.props.createEntry(entry);
    form.guestName.value = "";
    form.guestNum.value = "";
  }

  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      id: "addGuest"
    }, /*#__PURE__*/React.createElement("div", {
      className: "form",
      style: {
        display: 'flex'
      }
    }, /*#__PURE__*/React.createElement("form", {
      name: "addEntry",
      onSubmit: this.handleSubmit
    }, /*#__PURE__*/React.createElement(DisplayLabel, {
      for: "guestName",
      content: "Guest Name:"
    }), /*#__PURE__*/React.createElement(DisplayTextBox, {
      id: "guestName",
      name: "guestName",
      tbClass: "guestName"
    }), /*#__PURE__*/React.createElement(DisplayLabel, {
      for: "guestNum",
      content: "Phone Number:"
    }), /*#__PURE__*/React.createElement(DisplayTextBox, {
      id: "guestNum",
      name: "guestNum",
      tbClass: "guestNum"
    }), /*#__PURE__*/React.createElement(DisplayButton, {
      btnValue: "Add Guest",
      btnStyle: {
        float: 'right'
      },
      btnClass: "guestAdd"
    })))));
  }

}

class DisplayHomePage extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(DisplayButton, {
      id: "navigateBtn",
      btnValue: "Go to Wait List!",
      btnID: "Navigate",
      btnClass: "navigateBtn"
    }));
  }

}

class CustomersTable extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.delete = this.delete.bind(this);
    this.state = {
      delete: []
    };
  }

  delete(event) {
    console.log(event);
    const array = this.state.delete;
    array.push(event.target.id);
    this.setState({
      delete: array
    });
    console.log(array);
  }

  handleSubmit(e) {
    e.preventDefault();
    const form = document.forms.removeEntry;
    const array = this.state.delete;
    this.props.removeEntry(array);
  }

  render() {
    const customerRows = this.props.customers.map((customers, index) => /*#__PURE__*/React.createElement(CustomersRows, {
      onChange: this.delete,
      rowNum: index + 1,
      key: customers._id,
      customers: customers
    }));
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      id: "waitListDisplay"
    }, /*#__PURE__*/React.createElement("form", {
      name: "removeEntry",
      onSubmit: this.handleSubmit
    }, /*#__PURE__*/React.createElement(DisplayButton, {
      id: "removeBtn",
      btnValue: "Remove Guest",
      btnClass: "removeGuest"
    }), /*#__PURE__*/React.createElement("table", {
      width: "100%",
      align: "center"
    }, /*#__PURE__*/React.createElement(CustomersColumns, null), /*#__PURE__*/React.createElement("tbody", {
      id: "waitListBody",
      align: "center"
    }, customerRows)))));
  }

}

class DisplayCustomers extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      id: "waitListDisplayBtn"
    }, /*#__PURE__*/React.createElement(DisplayButton, {
      id: "DisplayWaitList",
      btnClass: "waitListDisplayBtn",
      btnValue: "Display Wait List"
    })));
  }

}

class CustomersColumns extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("th", null), /*#__PURE__*/React.createElement("th", null, "#"), /*#__PURE__*/React.createElement("th", null, "Guest Name"), /*#__PURE__*/React.createElement("th", null, "Guest Number"), /*#__PURE__*/React.createElement("th", null, "Date Added")));
  }

}

class CustomersRows extends React.Component {
  render() {
    const customers = this.props.customers;
    return /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "          ", /*#__PURE__*/React.createElement(DisplayCheckBox, {
      cbChange: this.props.onChange,
      cbValue: customers._id,
      cbClass: "delete",
      cbID: customers._id
    })), /*#__PURE__*/React.createElement("td", null, this.props.rowNum), /*#__PURE__*/React.createElement("td", null, customers.name), /*#__PURE__*/React.createElement("td", null, customers.number), /*#__PURE__*/React.createElement("td", null, customers.date.toLocaleString()));
  }

}

class DisplayFreeSlots extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      id: "homeMetric"
    }, /*#__PURE__*/React.createElement("div", {
      id: "cards__hover"
    }, /*#__PURE__*/React.createElement("svg", {
      className: "stroke"
    }, /*#__PURE__*/React.createElement("rect", {
      id: "stroke",
      rx: "8",
      ry: "8",
      x: "0",
      y: "0",
      width: "258",
      height: "134"
    })), /*#__PURE__*/React.createElement("svg", {
      className: "sheet_fd"
    }, /*#__PURE__*/React.createElement("rect", {
      id: "sheet_fd",
      rx: "8",
      ry: "8",
      x: "0",
      y: "0",
      width: "258",
      height: "134"
    }))), /*#__PURE__*/React.createElement(DisplayContent, {
      id: "metricLabel",
      content: "Available spots on waitlist:"
    }), /*#__PURE__*/React.createElement(DisplayContent, {
      id: "waitListCount",
      content: this.props.data
    })));
  }

}

const dateRegex = new RegExp('^\\d\\d\\d\\d-\\d\\d-\\d\\d');

function jsonDateReviver(key, value) {
  if (dateRegex.test(value)) return new Date(value);
  return value;
}

async function graphQLFetch(query, variables = {}) {
  try {
    const response = await fetch(window.ENV.UI_API_ENDPOINT, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        query,
        variables
      })
    });
    const body = await response.text();
    const result = JSON.parse(body, jsonDateReviver);

    if (result.errors) {
      const error = result.errors[0];

      if (error.extensions.code == 'BAD_USER_INPUT') {
        const details = error.extensions.exception.errors.join('\n ');
        alert(`${error.message}:\n ${details}`);
      } else {
        alert(`${error.extensions.code}: ${error.message}`);
      }
    }

    return result.data;
  } catch (e) {
    alert(`Error in sending data to server: ${e.message}`);
  }
}

class DisplayButton extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("div", {
      id: this.props.id
    }, /*#__PURE__*/React.createElement("input", {
      type: "submit",
      value: this.props.btnValue,
      id: this.props.btnID,
      style: this.props.btnStyle,
      className: this.props.btnClass
    }));
  }

}

class DisplayContent extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("div", {
      id: this.props.id
    }, /*#__PURE__*/React.createElement("span", null, this.props.content));
  }

}

class DisplayHeader extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      id: "header__default"
    }, /*#__PURE__*/React.createElement(DisplayContent, {
      id: "clock"
    }), /*#__PURE__*/React.createElement(DisplayContent, {
      id: "pagetitle",
      content: "Hotel California Reservation Management System - Home"
    })));
  }

}

class DisplayBody extends React.Component {
  constructor() {
    super();
    this.state = {
      customers: [],
      remainder: 0
    };
    this.createEntry = this.createEntry.bind(this);
    this.removeEntry = this.removeEntry.bind(this);
  }

  componentDidMount() {
    this.loadTable();
    this.loadRemainder();
  }

  async loadTable() {
    const query = `query{
            waitList{
                _id
                name
                number
                date
            }
        }`;
    const data = await graphQLFetch(query);

    if (data) {
      this.setState({
        customers: data.waitList
      });
    }
  }

  async loadRemainder() {
    const query = `query{
            remainder
        }`;
    const data = await graphQLFetch(query);

    if (data) {
      this.setState({
        remainder: data.remainder
      });
    }
  }

  async createEntry(entry) {
    let check = this.state.remainder;

    if (check > 0) {
      const query = `mutation entryAdd($entry: EntryInputs!){
                entryAdd(entry:$entry){
                    name
                }
            }`;
      const data = await graphQLFetch(query, {
        entry
      });

      if (data) {
        this.loadTable();
        this.loadRemainder();
        alert("Guest added to waitlist! ");
        console.log(this.state.remainder);
      }
    } else {
      alert("The guest waitlist is full, you cannot add more!");
    }
  }

  async removeEntry(entries) {
    let delete_num = entries.length;
    let customers = this.state.customers;

    if (customers.length == 0) {
      alert("No guests on waitlist!");
    } else if (delete_num == 0) {
      alert("Select a guest first!");
    } else {
      const query = `mutation entryRemove($entries: [ID!]!){
            entryRemove(entries:$entries){
                _id
            }
        }`;
      const data = await graphQLFetch(query, {
        entries
      });

      if (data) {
        this.loadTable();
        this.loadRemainder();
        alert("Guest removed from waitlist! ");
        console.log(this.state.remainder);
      }
    }
  }

  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      id: "Body"
    }, /*#__PURE__*/React.createElement(DisplayHomePage, null), /*#__PURE__*/React.createElement(DisplayFreeSlots, {
      data: this.state.remainder
    }), /*#__PURE__*/React.createElement(DisplayCustomers, null), /*#__PURE__*/React.createElement(CustomersTable, {
      customers: this.state.customers,
      removeEntry: this.removeEntry
    }), /*#__PURE__*/React.createElement(AddCustomer, {
      createEntry: this.createEntry
    })));
  }

}

class DisplayPage extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(DisplayHeader, null), /*#__PURE__*/React.createElement(DisplayBody, null));
  }

}

const element = /*#__PURE__*/React.createElement(DisplayPage, null);
ReactDOM.render(element, document.getElementById('content'));