

class DisplayLabel extends React.Component{
    render(){
        return(
            <label htmlFor = {this.props.for} > {this.props.content}</label>
        );

    }
}

class DisplayCheckBox extends React.Component {
    render() {
      return (
        <input
          type="checkbox"
          className={this.props.cbClass}
          id={this.props.cbID}
          value={this.props.cbValue}
          onChange={this.props.cbChange}
        />
      );
    }
  }

class DisplayTextBox extends React.Component{
    render(){
        return(
            <input type = "text" id = {this.props.id} name = {this.props.name} className = {this.props.tbClass} />
        );

    }
}

class AddCustomer extends React.Component{
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(e){
        e.preventDefault();
        const form = document.forms.addEntry;
        const entry = {
            name: form.guestName.value,
            number: form.guestNum.value,
            date: new Date(),
        }


        this.props.createEntry(entry);
        form.guestName.value=""; form.guestNum.value = "";
    }
    render(){
        return(
            <React.Fragment>
            <div id = "addGuest">
            <div className = "form" style={{display:'flex'}}>
            <form name = "addEntry" onSubmit={this.handleSubmit}>
            <DisplayLabel for = "guestName" content = "Guest Name:"/>
            <DisplayTextBox id = "guestName" name ="guestName" tbClass = "guestName" />
            <DisplayLabel for = "guestNum" content = "Phone Number:"/>
            <DisplayTextBox id = "guestNum" name = "guestNum" tbClass = "guestNum" />
            <DisplayButton btnValue = "Add Guest" btnStyle = {{float: 'right'}} btnClass = "guestAdd" />
            </form>
    
            </div>
        </div>
        </React.Fragment>
        );
    }

}


class DisplayHomePage extends React.Component{
    render(){
        return(
<React.Fragment>
    <DisplayButton id = "navigateBtn" btnValue = "Go to Wait List!" btnID = "Navigate" btnClass = "navigateBtn" />
</React.Fragment>
        );
    }
}

class CustomersTable extends React.Component{
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.delete = this.delete.bind(this);
        this.state={delete:[]}
    }
    delete(event){
        console.log(event)
        const array = this.state.delete;
        array.push(event.target.id);
        this.setState({delete:array});
        console.log(array);

    }
    handleSubmit(e){
        e.preventDefault();
        const form = document.forms.removeEntry;
        const array = this.state.delete;


        this.props.removeEntry(array);
    }
    render(){
        
        const customerRows = this.props.customers.map((customers, index)=> <CustomersRows onChange = {this.delete} rowNum={index+1} key={customers._id} customers = {customers}/>);
        return(
            
            <React.Fragment>
            <div id = "waitListDisplay">
                <form name = "removeEntry" onSubmit={this.handleSubmit}>
            <DisplayButton id = "removeBtn" btnValue = "Remove Guest" btnClass = "removeGuest"/>
            <table width = "100%" align = "center">
            <CustomersColumns/>
            <tbody id = "waitListBody" align="center">{customerRows}</tbody>
            </table>
            </form>
            </div>
            </React.Fragment>
        );
    }
}

class DisplayCustomers extends React.Component{
    render(){
        return(
            <React.Fragment>
            <div id = "waitListDisplayBtn">
            <DisplayButton id = "DisplayWaitList" btnClass = "waitListDisplayBtn" btnValue = "Display Wait List"/>
            </div>
            </React.Fragment>
        );
    }
}

class CustomersColumns extends React.Component{
    render(){
        return(
            <thead>
            <tr><th></th><th>#</th><th>Guest Name</th><th>Guest Number</th><th>Date Added</th></tr>
            </thead>
        );
    }
}

class CustomersRows extends React.Component{

    render(){
        const customers=this.props.customers
        return(
            <tr><td>          <DisplayCheckBox
            cbChange={this.props.onChange}
            cbValue={customers._id}
            cbClass="delete"
            cbID={customers._id}
          /></td><td>{this.props.rowNum}</td><td>{customers.name}</td><td>{customers.number}</td><td>{customers.date.toLocaleString()}</td></tr>
        );
    }
}


class DisplayFreeSlots extends React.Component{

    render(){
        return(
            <React.Fragment>
        <div id="homeMetric">
			<div id="cards__hover">
				<svg className="stroke">
					<rect id="stroke" rx="8" ry="8" x="0" y="0" width="258" height="134">
					</rect>
				</svg>
				<svg className="sheet_fd">
					<rect id="sheet_fd" rx="8" ry="8" x="0" y="0" width="258" height="134">
					</rect>
				</svg>
			</div>
            <DisplayContent id = "metricLabel" content = "Available spots on waitlist:" />
            <DisplayContent id = "waitListCount" content={this.props.data} />
            
		</div>
        </React.Fragment>

        );
    }
}

const dateRegex = new RegExp('^\\d\\d\\d\\d-\\d\\d-\\d\\d');

function jsonDateReviver(key, value){
    if (dateRegex.test(value)) return new Date(value);
    return value;
}


async function graphQLFetch(query, variables = {}){
    try{
        const response = await fetch(window.ENV.UI_API_ENDPOINT ,{
            method:'POST',
            headers:{'Content-Type':'application/json'},
            body:JSON.stringify({query, variables})
        });
        const body = await response.text();
        const result = JSON.parse(body, jsonDateReviver);

        if(result.errors){
            const error = result.errors[0];
            if(error.extensions.code == 'BAD_USER_INPUT'){
                const details = error.extensions.exception.errors.join('\n ');
                alert(`${error.message}:\n ${details}`);
            } else {
                alert(`${error.extensions.code}: ${error.message}`);
            }
        }
        return result.data;
    }
    catch (e){
        alert(`Error in sending data to server: ${e.message}`);

    }
}

class DisplayButton extends React.Component{
    render(){
        return(
            <div id = {this.props.id}>
                <input type = "submit" value = {this.props.btnValue} id = {this.props.btnID} style = {this.props.btnStyle} className = {this.props.btnClass}></input>
            </div>
        );
    }
}
class DisplayContent extends React.Component{
    render(){
        return(
            <div id = {this.props.id} ><span>{this.props.content}</span></div>
        );
    }
}

class DisplayHeader extends React.Component{
    render(){
        return(
            <React.Fragment>
            <div id = "header__default">
                <DisplayContent id = "clock" />
                <DisplayContent id = "pagetitle" content = "Hotel California Reservation Management System - Home"/>
                {/* <DisplayTitle />
                <DisplayClock /> */}
            </div>
            </React.Fragment>
        );
    }
}

class DisplayBody extends React.Component{
    constructor(){
        super();
        this.state = {
            customers:[],
            remainder:0
        };
        this.createEntry = this.createEntry.bind(this);
        this.removeEntry = this.removeEntry.bind(this);

    }
    componentDidMount(){
        this.loadTable();
        this.loadRemainder();
    }
    async loadTable(){
        const query=`query{
            waitList{
                _id
                name
                number
                date
            }
        }`;

        const data = await graphQLFetch(query);
        if(data){
            this.setState({customers: data.waitList})
        }
    }
    async loadRemainder(){
        const query=`query{
            remainder
        }`;

        const data = await graphQLFetch(query);
        if(data){
            this.setState({remainder: data.remainder})
        }
    }
    async createEntry(entry){
        let check = this.state.remainder;
        if (check > 0){
        const query = `mutation entryAdd($entry: EntryInputs!){
                entryAdd(entry:$entry){
                    name
                }
            }`;
        const data = await graphQLFetch(query, {entry});
        if(data){
            this.loadTable();
            this.loadRemainder();
            alert("Guest added to waitlist! ");
            console.log(this.state.remainder);
        }
    }
    else{

        alert("The guest waitlist is full, you cannot add more!");
    }
    }
    async removeEntry(entries){
        let delete_num = entries.length;
        let customers = this.state.customers;
        if (customers.length == 0){
        alert("No guests on waitlist!");
    }else if (delete_num == 0){
        alert("Select a guest first!");
    }
    else{
        const query = `mutation entryRemove($entries: [ID!]!){
            entryRemove(entries:$entries){
                _id
            }
        }`;
    const data = await graphQLFetch(query, {entries});
    if(data){
        this.loadTable();
        this.loadRemainder();
        alert("Guest removed from waitlist! ");
        console.log(this.state.remainder);
    }
    }
    }

    render(){
        return(
<React.Fragment>
<div id = "Body">
<DisplayHomePage />
<DisplayFreeSlots data={this.state.remainder}/>
<DisplayCustomers/>
<CustomersTable customers={this.state.customers} removeEntry={this.removeEntry}/>
<AddCustomer createEntry = {this.createEntry}/>
</div>
</React.Fragment>
        );
    }
}


class DisplayPage extends React.Component{
    render(){
        return(
            <React.Fragment>
               <DisplayHeader />
               <DisplayBody/>
            </React.Fragment>
        );
    }
}

const element = <DisplayPage />;

ReactDOM.render(element, document.getElementById('content'));