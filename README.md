Git repository: https://bitbucket.org/hotprawnstar/it5007-tutorial/

To clone git repo:
git clone https://russell__wong@bitbucket.org/hotprawnstar/it5007-tutorial.git

UI hosted on port 3000 and backend hosted on port 5000

To start the application, the database has to be launched first followed by the backend then the front end.


To start database run the following commands:
1. cd api
2. mongod


To start backend run the following commands:
1. cd api
2. npm install
3. npm start

To start front end run the following commands:
1. cd ui
2. npm install
3. npm start


To initialize database, run initialization script in api folder:
mongo waitinglist scripts/init.mongo.js

To test CRUD operations, run test script in api folder:
node scripts/trymongo.js